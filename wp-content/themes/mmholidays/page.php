<?php 
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
**/ 
get_header(); ?>
	<div id="primary" class="content-area">
		<div class="row">
			<div class="col-md-9 pages">
				 <div class="defaultpage">
				<?php 
		            while ( have_posts() ) : the_post();
		            	if($post->ID == MH_AIR_TICKETING) {
		            		$args = array(
						        'post_type' => 'page',
						        'post_parent' => MH_AIR_TICKETING
						    );
						    $air_childs = get_posts( $args );
						    foreach ($air_childs as  $air_child) {
						    	echo '<h3>'.$air_child->post_title.'</h3>';
						    	echo $air_child->post_content;
						    }
		            	}else {
		            		the_content(); 
		            	}		                
		           	endwhile;
		        ?>
		        </div>
			</div>
			<?php get_sidebar();?>
		</div>	       
	</div><!-- .content-area -->
<?php get_footer(); ?>
