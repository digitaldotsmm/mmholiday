<?php
/*
 * Template Name:Reservation
 */
?>
<?php get_header();?>
<div id="primary" class="content-area">
   <div class="row">
      <div class="col-md-9">
			   <div class="booking-form-wrapper">
               <div class="booking-form">
                  <div id="success_reservation" class="alert alert-success"><h4>Thanks you for your reservation. One of our staff will contact you shortly.</h4></div>
                  <!--#success-->
                  <form id="tour-book" action="" method="post" novalidate="novalidate">
                     <ul>
                        <li class="tour_info_title">
                           <h2>Tour Information</h2>
                        </li>
                        <li>
                           <label for="title"><strong>Tour Programs<span class="red-text">*</span> :</strong></label>
                           <select name="tour_title" id="tour_title" class="selected-tour required">
                              <option value="">Select Tour Program</option>                              
                              <?php 
                                 $args=array(
                                     'post_type'     => MH_TOUR_PROGRAM,
                                     'post_status'   => 'publish',
                                     'order_by'      => 'post_title',
                                     'posts_per_page' => -1,
                                     'order'         => 'ASC'
                                 );
                                 $tours = get_posts($args);
                                 foreach ($tours as $tour) : 
                                 ?> 
                              <option value="<?php echo $tour->post_title; ?>"<?php echo ( isset($_POST['tour_name']) && $_POST['tour_name'] == $tour->ID ) ? ' selected="selected"' : ''; ?>><?php echo $tour->post_title; ?></option>
                              <?php endforeach; ?>
                           </select>
                        </li>
                        <li>
                           <label for="interested_places"><strong>Other Interested Places :</strong></label>
                           <textarea name="interested_places" id="interested_places" class="input-textarea"></textarea>
                        </li>
                        <li>
                           <label for="arrivaldate"><strong>Date of Arrival<span class="red-text">*</span>:</strong></label>
                           <input type="text" name="arrivaldate" id="arrivaldate" value="" class="date-picker required input-text">                       
                        </li>
                        <li>
                           <label for="depaturedate"><strong>Date of Departure<span class="red-text">*</span>:</strong></label>
                           <input type="text" name="depaturedate" id="depaturedate" value="" class="date-picker required input-text">                       
                        </li>
                        <li>
                           <label for="no_pax"><strong>Number of Pax :</strong></label>                          
                           <p>
                              <span class="select-wrap">
                                 <label for="adult">Adult: </label>
                                 <select name="adult" id="adult" class="reqiured">
                                    <?php for($a=1;$a <= 100;$a++) : ?>
                                    <option value="<?php echo $a; ?>"><?php echo $a; ?></option>
                                    <?php endfor; ?>                                                         
                                 </select>
                              </span>                             
                              <span class="select-wrap">
                                 <label for="child">Children: </label>
                                 <select name="child" id="child" class="">
                                    <option value="null">None</option>
                                     <?php for($c=1;$c <= 100;$c++) : ?>
                                    <option value="<?php echo $c; ?>"><?php echo $c; ?></option>
                                    <?php endfor; ?>                                                         
                                 </select>
                              </span>
                              <span class="select-wrap">
                                 <label for="infants">Infants: </label>
                                 <select name="infants" id="infants" class="reqiured">
                                    <option value="null">None</option>
                                    <?php for($i=1;$i <= 100;$i++) : ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php endfor; ?>                                                         
                                 </select>
                              </span>
                           </p>
                        </li>
                        <li>
                           <label for="title"><strong>Hotel accommodation :</strong></label>
                           <p class="gender-option-wrapper">
                              <span class="radio-wrap">
                              <input type="radio" value="Yes" name="hotel_accommodation" id="hotel_acco_yes">
                              <label class="label-fl" for="hotel_acco_yes">Yes</label>
                              </span>
                              <span class="radio-wrap">
                              <input type="radio" value="No" name="hotel_accommodation" id="hotel_acco_no">
                              <label class="label-fl" for="hotel_acco_no">No</label>
                              </span>
                           </p>
                           <label for="title"></label>
                           <p class="no-label">
                              <span class="select-wrap">
                                 <label for="hotel_star">Star Rate : </label>
                                 <select name="hotel_star" id="hotel-star" class="reqiured">
                                    <option value="">Select</option>
                                    <?php for($s=1;$s <= 100;$s++) : ?>
                                     <option value="<?php echo $s; ?>"><?php echo $s; ?></option>
                                    <?php endfor; ?>    
                                 </select>
                              </span>
                              <span class="inline-input">
                              <label for="">Target Rate :</label><br/> <br/>
                              <input type="text" name="target_rate" id="target-rate" value="" class="input-text hasDatepicker"> 
                              <label class="us-currency">US$ </label>
                              </span>
                           </p>
                        </li>
                        <li>
                           <label for="flight_des"><strong>Flight to and from Destination(s) :</strong></label>
                           <p class="gender-option-wrapper">
                              <span class="radio-wrap">
                              <input type="radio" value="Yes" name="flight_des" id="flight_des_yes">
                              <label class="label-fl" for="flight_des_yes">Yes</label>
                              </span>
                              <span class="radio-wrap"><input type="radio" value="No" name="flight_des" id="flight_des_no">
                              <label class="label-fl" for="flight_des_no">No</label>
                              </span>
                           </p>
                        </li>
                        <li>
                           <label for="tour_guide"><strong>Tour Guide :</strong></label>
                           <select name="tour_guide" class="">
                              <option value="">Select</option>
                              <option value="Yes">Yes</option>
                              <option value="No">No</option>
                           </select>
                           <p class="no-label tour_guide_type">
                              <span class="radio-wrap">
                              <input type="radio" value="Station Guide" name="guide_type" id="station_guide">
                              <label class="label-fl" for="station_guide">Station Guide</label>
                              </span>
                              <span class="radio-wrap">
                              <input type="radio" value="Throughout Guide" name="guide_type" id="throughout_guide">
                              <label class="label-fl" for="throughout_guide">Throughout Guide </label>
                              </span>
                           </p>
                        </li>
                        <li>
                           <label for="lang_require"><strong>Language Requirement :</strong></label>
                           <p class="lang-option-wrapper">
                              <span class="radio-wrap">
                              <input type="checkbox" name="lang_require[]" id="lang_english" value="English" class="input-text"> 
                              <label class="label-fl" for="lang_english">English</label>
                              </span>
                              <span class="radio-wrap">
                              <input type="checkbox" name="lang_require[]" id="lang_italian" value="Italian" class="input-text"> 
                              <label class="label-fl" for="lang_italian">Italian</label>
                              </span>
                              <span class="radio-wrap">
                              <input type="checkbox" name="lang_require[]" id="lang_french" value="French" class="input-text"> 
                              <label class="label-fl" for="lang_french">French</label>
                              </span>
                              <span class="radio-wrap">
                              <input type="checkbox" name="lang_require[]" id="lang_german" value="German" class="input-text"> 
                              <label class="label-fl" for="lang_german">German</label>
                              </span>
                              <br/>
                              <span class="radio-wrap">
                              <input type="checkbox" name="lang_require[]" id="lang_spanish" value="Spanish" class="input-text"> 
                              <label class="label-fl" for="lang_spanish">Spanish</label>
                              </span>
                              <span class="radio-wrap">
                              <input type="checkbox" name="lang_require[]" id="lang_japanese" value="Japanese" class="input-text"> 
                              <label class="label-fl" for="lang_japanese">Japanese</label>
                              </span>
                              <span class="radio-wrap">
                              <input type="checkbox" name="lang_require[]" id="lang_chinese" value="Chinese" class="input-text"> 
                              <label class="label-fl" for="lang_chinese">Chinese</label>
                              </span>
                              <span class="radio-wrap">
                              <input type="checkbox" name="lang_require[]" id="lang_russian" value="Russian" class="input-text"> 
                              <label class="label-fl" for="lang_russian">Russian</label>
                              </span>
                           </p>
                        </li>
                        <li class="tour_info_title">
                           <h2>Personal Information</h2>
                        </li>
                        <li>
                           <label for="title"><strong>Title <span class="red-text">*</span> :</strong></label>
                           <select name="title" id="title" class="reqiured">
                              <option value="Mr">Mr.</option>
                              <option value="Mrs">Mrs.</option>
                              <option value="Ms">Ms.</option>
                              <option value="Dr">Dr.</option>
                           </select>
                        </li>
                        <li>
                           <label for="contactName"><strong>Full Name <span class="red-text">*</span> :</strong></label>
                           <input type="text" name="contactName" id="contactName" value="" class="required input-text">                       
                        </li>
                        <li>
                           <label for="email"><strong>Email <span class="red-text">*</span> :</strong></label>
                           <input type="text" name="email" id="email" value="" class="required input-text email">                       
                        </li>
                        <li>
                           <label for="phone"><strong>Phone Number <span class="red-text">*</span> :</strong></label>
                           <input type="text" name="phone" id="phone" value="" class="required input-text phone">                       
                        </li>
                        <li>
                           <label for="nationality"><strong>Nationality :</strong> </label>
                           <input type="text" name="nationality" id="nationality" value="" class="input-text">                       
                        </li>
                        <li>
                           <label><strong>Gender :</strong></label>
                           <p class="gender-option-wrapper">
                              <span class="radio-wrap">
                                 <input type="radio" value="male" name="sex" id="gender_male">
                                 <label class="label-fl" for="gender_male">
                                    Male
                              </span>
                              <span class="radio-wrap"><input type="radio" value="female" name="sex" id="gender_female"><label class="label-fl" for="gender_female">Female</span>
                           </p>
                        </li>
                        <li>
                        <label for="address"><strong>Address :</strong></label>
                        <textarea name="address" id="address" class="input-textarea"></textarea>
                        </li>
                        <li>
                           <label for="passportno"><strong>Passport No. :</strong></label>
                           <input type="text" name="passportno" id="passportno" value="" class="input-text">                       
                        </li>
                        <li>
                           <label for="add_msg"><strong>Additional Message :</strong></label>
                           <textarea name="add_msg" id="add_msg" class="input-textarea"></textarea>
                        </li>
<!--                        <li>
                           <label for="email-copy"><strong>Email Copy for this message : </strong></label>
                           <p class="gender-option-wrapper">
                              <span class="radio-wrap">
                              <input type="checkbox" id="email-copy" name="email_copy" value="yes">
                              </span>
                           </p>
                        </li>-->
                        <li>
                           <label for="email-copy"><strong>Security question: </strong></label>  
                           <p class="captcha-wrap">
                              <img id="friend-captcha" src="<?php echo TEMPLATE_URL; ?>/includes/coolcaptcha/captcha.php">
                              <a id="change-image" onclick="
                                 document.getElementById('friend-captcha').src='<?php echo TEMPLATE_URL; ?>/includes/coolcaptcha/captcha.php?'+Math.random();
                                 document.getElementById('friend-captcha-form').focus();return false;" href="#">Not readable? Change text.</a>
                              <br>
                              <input type="text" placeholder="Enter Security Code" class="required" autocomplete="off" id="friend-captcha-form" name="captcha">           
                           </p>
                        </li>
                        <li>
                           <input class=" btn-success formButton" name="submit" type="submit" id="submit" value="Book Now">
                        </li>
                     </ul>
                  </form>
               </div>   
            </div>
         </div>
			<?php get_sidebar(); ?>	
	</div>	
</div>	
<?php get_footer();?>