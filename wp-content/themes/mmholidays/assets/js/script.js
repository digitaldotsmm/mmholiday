/*!
 *      Author: DigitalDots
	name: script.js	
	requires: jquery	
 */
var ddFn = {
    
    init: function(){
        Global = new this.Global();
        Utils = new this.Utils();     
        Tour = new this.Tour();   
        
        Global.__init();        
    },
    

    Global:function(){        
        
        this.__init = function(){  
            // justify height of #hotelstab
            // if( $('#tourtab').length > 0 ){
            //     $('#tourtab').easyResponsiveTabs({
            //        activate: function(){                   
            //            Utils.justify_height('#tourtab .resp-tab-content-active', '.each_hotel');
            //        } 
            //     });

            //     Utils.justify_height('#tourtab .resp-tab-content-active', '.each_hotel');
            // }           
        }
      
        this.home_url = function(){
            var home_page_url = window.location.protocol + "//" + window.location.host + "/";     
            return home_page_url;
        }
        this.template_url = function(){
            return this.home_url() + 'wp-content/themes/goldenwings/';           
        }        
        this.ajax_url = function(){
            var ajax_url = this.home_url() + "wp-admin/admin-ajax.php";        
            return ajax_url;
        }  

          $('#left-slider').tab('show');
                      
    }, // end of Global

     Tour:function(){        
        
        this.__init = function(){             
        }
      
        this.home_url = function(){
            var home_page_url = window.location.protocol + "//" + window.location.host + "/";     
            return home_page_url;
        }
        this.template_url = function(){
            return this.home_url() + 'wp-content/themes/goldenwings/';           
        }        
        this.ajax_url = function(){
            var ajax_url = this.home_url() + "wp-admin/admin-ajax.php";        
            return ajax_url;
        }              
    }, // end of Global

    Utils: function(){
        //this.exist= false;
        this.validate_email = function (email){	
            var filter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            //var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
            if(filter.test(email)){
                return true;
            }
            else{
                return false;
            }
        };	
	
        this.format_my_date = function(dateStr) {
            return dateStr.replace(/^([a-z]{3})( [a-z]{3} \d\d?)(.*)( \d{4})$/i, '$1,$2$4$3');
        };
	
        this.remove_GMTorUTC = function(dateStr) {
            var noGMT = dateStr.replace(/^(.*)(:\d\d GMT+)(.*)$/i, '$1');  // Trim string like 'Thu Apr 21 2011 14:08:46 GMT+1000 (AUS Eastern Standard Time)'
            var noUTC = noGMT.replace(/^(.*)(:\d\d UTC+)(.*)$/i, '$1');    // Trim string like 'Thu Apr 21 14:08:46 UTC+1000 2011'
            return noUTC;
        };
        
        this.is_user_email_exist = function(email, callback){                 
            $.ajax({                                                     
                url: Global.ajax_url(),
                type:"post",                        
                data: {
                    action   :  'is_user_email_exist',
                    'email'  :   email     
                },
                async: false,
                dataType:"json",
                success:function(response){                          
                    if(response.is_exist){
                        return callback (true);
                    }else{
                        return callback (false);
                    }
                }
            });            
        }; 
        this.is_user_name_exist = function(username, callback){              
            $.ajax({                                                     
                url: Global.ajax_url(),
                type:"post",                        
                data: {
                    action   :  'is_user_name_exist',
                    'username'  :   username     
                },
                async: false,
                dataType:"json",
                success:function(response){                          
                    if(response.is_exist){                          
                        return callback (true);
                    }else{
                        return callback(false);
                    }
                }
            });            
        };
        this.is_user_pass_valid = function(username, userpass, callback){           
            $.ajax({                                                     
                url: Global.ajax_url(),
                type:"post",                        
                data:  {
                    action : "is_password_exist", 
                    'id'   : username,
                    'pass' : userpass
                }, 
                async: false,
                dataType: "json",
                success:function(response){                          
                    if(response){                                 
                        return callback (true);
                    }else{                         
                        return callback(false);
                    }
                }
            });            
        };
        this.is_user_logged_in = function(callback){
            $.ajax({                                                     
                url: Global.ajax_url(),
                type:"post",                        
                data: {
                    action   :  'is_user_logged_in'                     
                },
                async: false,
                dataType:"json",
                success:function(response){                       
                                               
                    return callback (response.is_logged_in);
                     
                }
            });      
        }                 
    }

};


var Global;
var Utils;
var Tour;

$ = $.noConflict();
$(document).ready(function(){  
    ddFn.init();

      // menu
        if ($(window).width() > 768) {
            $('#menu-main-menu .dropdown').on('mouseover', function(){
                $('.dropdown-toggle', $(this)).next('.dropdown-menu').show();
            }).on('mouseout', function(){            
                $('.dropdown-toggle', $(this)).next('.dropdown-menu').hide();
            });
            
            $('.dropdown-toggle').click(function() {
                if ($(this).next('.dropdown-menu').is(':visible')) {
                    window.location = $(this).attr('href');
                }
            });
        }
        else {
            $('.menu-main-menu .dropdown').off('mouseover').off('mouseout');
        }

      $('.flexslider').flexslider({
        animation: "slide",
        animationLoop: false,
        itemWidth: 119,
        itemMargin: 20,
        pausePlay: true,
        
      });
      $('.testimonial_flexslider').flexslider({
        animation: "slide",
        animationLoop: false,
        // itemWidth: 119,
        itemMargin: 30,
        pausePlay: true,
        
      });


          //create the slider
          $('.cd-testimonials-wrapper').flexslider({
            selector: ".cd-testimonials > li",
            animation: "slide",
            controlNav: false,
            slideshow: false,
            smoothHeight: true,
            start: function(){
              $('.cd-testimonials').children('li').css({
                'opacity': 1,
                'position': 'relative'
              });
            }
          });


        // date picker
    $('.date-picker').datepicker({
        dateFormat: 'dd / M / yy'
    });
        //pagination li 
    $('ul.pagination a').wrap('<li></li>');
    $('ul.pagination span').wrap('<li></li>');

    // Select first tab
	$('.nav-tabs a:first').tab('show') 

        // //ROLL ON HOVER
    $(function() {
        $(".mask").css("opacity","0");
        $(".mask").hover(function () {
            $(this).stop().animate({
                opacity: .7
            }, "slow");
        },
        function () {
            $(this).stop().animate({
            opacity: 0
            }, "slow");
        }); 
    });
    
    $(function() {
          fakewaffle.responsiveTabs(['xs', 'sm']);
      });

    //table wrap
    $('table').wrap( "<div class='table-responsive'></div>" );
    $('table').addClass('table');
    // Tour Booking form submit    
    var tour_bookingForm = $("#tour-book");

    tour_bookingForm.submit(function(e){
        e.preventDefault();

        if ( do_form_validation(tour_bookingForm) ){
   
            var formData = tour_bookingForm.serializeArray();
   
            formData.push({
                name: 'action',
                value: 'sendmail'
            });

            // send data via ajax
            $.ajax({                                                     
                url: '/wp-admin/admin-ajax.php',
                type:"post",                        
                data: formData,
                async: false,     
                dataType : 'json',
                success:function(response){                                                 
                    if( response.status == 'yes' ){
                        tour_bookingForm.fadeOut('slow');
                        $("#success_reservation").fadeIn();
                    }
                }
            });         

        }
    });


    jQuery("a[rel^='prettyPhoto']").prettyPhoto({
        social_tools: false,
        animation_speed: 'normal', // fast/slow/normal 
        slideshow: 5000, // false OR interval time in ms
        autoplay_slideshow: false, // true/false
        opacity: 0.80, // Value between 0 and 1 
        show_title: true, // true/false            
        allow_resize: true, // Resize the photos bigger than viewport. true/false
        default_width: 500,
        default_height: 344,
        counter_separator_label: '/', // The separator for the gallery counter 1 "of" 2
        theme: 'pp_default', // light_rounded / dark_rounded / light_square / dark_square / facebook
        horizontal_padding: 20, // The padding on each side of the picture 
        hideflash: false, // Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto
        wmode: 'opaque', // Set the flash wmode attribute
        autoplay: true, // Automatically start videos: True/False 
        modal: false, // If set to true, only the close button will close the window
        deeplinking: true, // Allow prettyPhoto to update the url to enable deeplinking. 
        overlay_gallery: true, // If set to true, a gallery will overlay the fullscreen image on mouse over 
        keyboard_shortcuts: true, // Set to false if you open forms inside prettyPhoto 
        changepicturecallback: function () {}, // Called everytime an item is shown/changed 
        callback: function () {} // Called when prettyPhoto is closed
    });
    

    // festival and hotel
    // if ( $('#tourtab').length > 0 ) {
    //     var type_action = $('#tourtab').attr('data-type'); 
    //     $("#tourtab").easyResponsiveTabs({
    //         type: type_action, //Types: default, vertical, accordion           
    //         width: 'auto', //auto or any custom width
    //         fit: true,   // 100% fits in a container
    //         closed: false, // Close the panels on start, the options 'accordion' and 'tabs' keep them closed in there respective view types
    //         activate: function() {},  // Callback function, gets called if tab is switched
    //         tabidentify: 'tab_identifier_child', // The tab groups identifier *This should be a unique name for each tab group and should not be defined in any styling or css file.
    //         activetab_bg: '#B5AC5F', // background color for active tabs in this group
    //         inactive_bg: '#E0D78C', // background color for inactive tabs in this group
    //         active_border_color: '#9C905C', // border color for active tabs heads in this group
    //         active_content_border_color: '#9C905C' // border color for active tabs contect in this group so that it matches the tab head border
    //     });
    // }

});


    $(window).load(function() {      
        var w;    
        w = $('.inbound-tours-item').width();
        $(".triangle-bottomleft").css({"border-right-width": w});
        $('.inbound-tours-item').matchHeight({});
    });
    $( window ).resize(function() {
        var w;
        w = $('.inbound-tours-item').width();
        $(".triangle-bottomleft").css({"border-right-width": w});
        $('.inbound-tours-item').matchHeight({});
    });

// Do form Validation
function do_form_validation($fm){     
    var valid = true; 
    var $ele = null;      
    var $val = null;           

    $fm.find('.required').each(function(){                   
              
        $ele = jQuery(this); 
        
        $val = $ele.val();   
        
        // reset all input to normal
        $ele.removeClass('invalid');      
                
        // if ther is no vale or just space
        if( $val.length <= 0){               
            $ele.addClass('invalid');
            valid = false;   
        }  
         
        if( $ele.hasClass('phone') && $val != '' ){
            var $Regex = /^[0-9-+]+$/;
            if ( !$Regex.test($val) ){
                $ele.addClass('invalid');                        
                valid = false;
            }                   
        }               
       
                
        if( $ele.hasClass('email') && $val != '' ){                      
            if( !validate_email($val)){                                               
                $ele.addClass('invalid');                        
                valid = false;
            }
        }                     
        
        if( $ele.hasClass('captcha') && $val != '' ){
            is_captcha_correct($val, function(result){                        
                if ( result == 'no'){
                    $ele.addClass('invalid');                          
                    valid =  false;
                }       
            });
        }
    });
    
    if( valid ) return true;
    return false;         
            
}     

function validate_email(email){
    var filter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    //var filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9]+[a-zA-Z0-9.-]+[a-zA-Z0-9]+.[a-z]{2,4}$/;
    if(filter.test(email)){
        return true;
    }
    else{
        return false;
    }
}

function is_captcha_correct (value, callback){
    $.ajax({                                                     
        url: '/wp-admin/admin-ajax.php',
        type:"post",                        
        data: {
            action      :  'check_captcha',
            'value'     : value
        },
        async: false,
        dataType:"json",
        success:function(response){                   

            return callback (response.valid);

        }
    });      
}