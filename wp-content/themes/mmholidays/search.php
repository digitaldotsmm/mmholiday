<?php 
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
**/ 
get_header(); 
$pagetitle = sprintf( __( 'Search Results for: &ldquo;%s&rdquo;', THEMENAME ), '<span>' . get_search_query() . '</span>' );
?>
	<div id="primary" class="content-area">
		<div class="row">
			<div class="col-md-9 pages">
				 <div class="defaultpage">
					<h2><?php echo $pagetitle; ?></h2>
					
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
				
						<h4><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
						
						<p><?php echo string_limit_words(strip_tags(get_the_content()), 30) . '...'; ?></p>
						<a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">View More</a>
						<hr/>
					<?php endwhile; else: ?>
						<p>
							<?php _e('Sorry, no posts matched your criteria.'); ?>
						</p>
					<?php endif; ?>
		        </div>
			</div>
			<?php get_sidebar();?>
		</div>	       
	</div><!-- .content-area -->
<?php get_footer(); ?>
