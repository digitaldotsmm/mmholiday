<div id="sidebar" class="col-md-3">
	<?php
        if(is_singular(MH_DESTINATION)) {
            dynamic_sidebar('destinations-widget_area');
        }
        elseif(is_singular(MH_TOUR_PROGRAM)){
            dynamic_sidebar('tour_sidebar-widget_area');
        }
        else {
        	dynamic_sidebar('main-sidebar-widget-area');
        }
	?>
</div>

