<?php get_header(); ?>	

<div id="primary" class="content-area">	
    <div class="row">
        <div class="col-md-9">
            <div class="tours-tabs">
                <ul class="nav nav-tabs responsive">
                <?php 
                    $tour_code=get_field('tour_code',$post->ID,true);
                    $tour_duration=get_field('tour_duration',$post->ID,true);
                    $tour_route=get_field('tour_route',$post->ID,true);
                    $content=$post->post_content;
                    $itinerary=get_field('day_programs',$post->ID,true);
                    $gallery=get_field('gallery',$post->ID,true);
                    $price_in=get_field('our_price_include',$post->ID,true);
                    $price_ex=get_field('our_price_exclude',$post->ID,true);
                    $accommodation=get_field('accommodation',$post->ID,true);
                    $terms_and_conditons=get_field('terms_and_conditons',$post->ID,true);
                ?>
                  <?php if($content || $tour_code || $tour_duration || $tour_route) { ?><li><a data-toggle="tab" href="#detail">Detail</a></li> <?php } ?>
                  <?php if($itinerary) { ?><li><a data-toggle="tab" href="#itinerary">Itinerary</a></li><?php } ?>
                  <?php if($gallery) { ?><li><a data-toggle="tab" href="#gallery">Gallery</a></li><?php } ?>                         
                  <?php if($accommodation) { ?><li><a data-toggle="tab" href="#accommodation">Accommodation</a></li> <?php } ?>
                  <?php if($terms_and_conditons) { ?><li><a data-toggle="tab" href="#tac">Terms and Conditons</a></li> <?php } ?>
                </ul>

                <div class="tab-content responsive">
                    <?php if($content || $tour_code || $tour_duration || $tour_route ) { ?>
                        <div id="detail" class="tab-pane fade in">
                            <div class="tours-info">
                                <?php if($tour_code) { ?>
                                <div class="each-info">
                                    <div class="info-title">Tour Code</div>
                                    <div class="info-text"><?php echo $tour_code;?></div>
                                </div>
                                <?php } ?>
                                <?php if($tour_duration) { ?>
                                <div class="each-info">
                                    <div class="info-title">Tour Duration</div>
                                    <div class="info-text"><?php echo $tour_duration;?></div>
                                </div>
                                <?php } ?>
                                <?php if($tour_route) { ?>
                                <div class="each-info">
                                    <div class="info-title">Tour Route</div>
                                    <div class="info-text"><?php echo $tour_route;?></div>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="tours-tabs_content">
                                <?php echo apply_filters('the_content',$content);?>

                                <?php if($price_in) { ?>
                                    <div class="price_in">
                                        <h4>Our Price Included</h4>
                                        <?php echo $price_in;?>
                                    </div>                                   
                                 <?php }?>

                                <?php if($price_ex) { ?>
                                    <div class="price_ex">
                                        <h4>Our Price Excluded</h4>
                                        <?php echo $price_ex;?>
                                    </div>
                                <?php }?>
                                
                            </div>
                        </div>
                    <?php } ?>

                    <?php if($itinerary) { ?>
                        <div id="itinerary" class="tab-pane fade">
                            <div class="tours-tabs_content">
                                <div class="tour_itinerary">
                                    <?php  if($itinerary):foreach ($itinerary as $item_key => $item): ?>
                                        <div class="each_item">
                                            <div class="item_icon_wrap">
                                                <div class="item_icon">
                                                    <div class="item_text">
                                                        <?php echo $item_key+1;?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item_content">
                                                <h3 class="item_title">Day <?php echo $item_key+1;?> - <?php echo $item['title'];?></h3>
                                                <div class="item_description">
                                                    <?php $item_img=aq_resize($item['image'],400,300,true,true,true);?>
                                                    <figure>
                                                        <a href="<?php  echo $item['image']; ?>" title="<?php echo $item['title'];?>" rel="prettyPhoto[daygal]"  class="prettyPhoto">
                                                            <img src="<?php echo $item_img;?>" title="<?php echo $item['title'];?>" class="img-responsive" />
                                                        </a>
                                                    </figure>
                                                    <?php echo $item['content'];?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; endif;?>
                                </div>                                
                            </div>
                        </div>
                    <?php } ?>

                    <?php if($gallery) { ?>
                        <div id="gallery" class="tab-pane fade in">
                            <div class="tours-tabs_content tour_gallery">
                                <div class="row">
                                    <?php 
                                        $tour_gals=get_field('gallery',$post->ID);              
                                        if ($tour_gals):          
                                            foreach($tour_gals as $tour_gal):                               
                                                $gal_imgs=aq_resize($tour_gal["url"],390,264,true,true,true);
                                    ?>                                                                                          
                                                <div class="col-md-4 col-sm-6 col-xs-12">
                                                    <div class="gallery-item">
                                                        <a href="<?php echo $tour_gal["url"]; ?>" rel="prettyPhoto[tourgal]" class="prettyPhoto">
                                                            <img src="<?php echo $gal_imgs;?>" alt="<?php echo $tour_gal['title']?>" class="img-responsive thumbnail">
                                                        </a>                           
                                                    </div>
                                                </div>    
                                    <?php endforeach;    endif;?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>                    

                    <?php if($accommodation) { ?>
                        <div id="accommodation" class="tab-pane fade in">
                            <div class="tours-tabs_content">
                                <?php echo $accommodation;?>
                            </div>
                        </div>
                    <?php } ?>

                    <?php if($terms_and_conditons) { ?>
                        <div id="tac" class="tab-pane fade">
                            <div class="tours-tabs_content">
                                <?php echo $terms_and_conditons;?>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div> <!-- end tour tabs -->
            <div class="single_book">
                <form action="<?php echo get_permalink(MH_RESERVATION); ?>" method="post">                           
                    <input type="hidden" value="<?php echo $post->ID; ?>" name="tour_name" >
                    <input class="single_form btn read-more" name="submit" type="submit" id="submit" value="Book this tour">
                </form>
            </div>
        </div>
        <?php get_sidebar();?>
        
    </div>
</div><!-- .content-area -->
<?php get_footer(); ?>