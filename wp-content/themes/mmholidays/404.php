<?php 
/**
 * The template for 404 displaying pages
 *
**/ 
get_header(); ?>
	<div id="primary" class="content-area">
		<div class="row">
			<div class="col-md-9 pages">
				 <div class="defaultpage">
					<h2><?php _e("Search Results"); ?></h2>	
					<?php if( $_REQUEST['calcat'] ) : ?>
						<p>No event is found.</p>
					<?php else: ?>
						<p><?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.' ); ?></p>
						<?php get_search_form(); ?>	
					<?php endif; ?>
					<div class="clear"></div>
		        </div>
			</div>
			<?php get_sidebar();?>
		</div>	       
	</div><!-- .content-area -->
<?php get_footer(); ?>
