<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" class="searchform">
    <label for="kwd" class="hidden">Search</label>
    <input type="text" name="s" id="s" class="input-search clearInput" value="" title="Site search" placeholder="Search here" />
    <input type="submit" id="searchsubmit" class="button btn-search" value="Search" >
</form>

