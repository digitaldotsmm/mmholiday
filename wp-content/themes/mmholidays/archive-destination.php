<?php
/**
 * Template Name: Destinations
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>
	<div id="primary" class="content-area">
		<div class="row">
			<div class="col-md-12 destination-list">
                <?php
                	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
					if ( have_posts() ) :
						$counter = 0;
						echo  '<div class="row">';
	                	while ( have_posts() ): the_post();
							if(($counter % 4 ==0) && ($counter > 0)){
								echo '</div><div class="row">';
							 }
							$img2=get_attachment_image_src($des_post['ID'], 'full');
			        		$des_img=aq_resize($img2[0],450,290,true,true,true);
				?>
                    		<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                <div class="destination-item">
                                    <div class="destination-image-wrapper">
                                        <a title="<?php the_title(); ?>" href="<?php the_permalink(); ?>"> <img src="<?php echo $des_img; ?>" /></a>
                                    </div>
                                    <div class="destination-content-wrapper">
                                        <h4 class="entry-title"><?php the_title(); ?></h4>
                                        <div class="tours-excerpt"><?php echo wp_trim_words( get_the_content(), 15 ); ?></div>    
                                        <div class="des_more"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="read-more">Read More ...</a>   </div>                                   
                                     </div>       
                                </div>
                                
                            </div>
                <?php $counter++; endwhile;?> 
                	<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
				        <nav aria-label="Page navigation">
				            <ul class="pagination">
				                <?php dd_pagination(); ?>
				            </ul>
				        </nav>
				    </div>	
        		<?php endif; wp_reset_query();  ?>
			</div>
		</div>	        
	</div>
<?php get_footer(); ?>