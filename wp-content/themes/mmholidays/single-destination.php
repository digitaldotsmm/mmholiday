<?php get_header(); ?>
    <div id="primary" class="content-area">
        <div class="row">
            <div class="col-md-9">
                <div class="post_content">
                    <?php 
                        while ( have_posts() ) : the_post();
                            the_content(); 
                        endwhile;
                    ?>
                </div>   
                <!-- key attraction  -->
                <?php 
                    $key_attractions = get_field('key_attractions');
                    if( $key_attractions ) : 
                ?>           
                    <div class="attractions">
                        <h3>Key Attractions</h3>
                        <ul class="list-block">
                           <?php
                                foreach( $key_attractions as $key_attraction ) : 
                                    echo '<li>'.$key_attraction['title'].'</li>';
                                endforeach; 
                            ?> 
                        </ul>
                    </div>
                <?php  endif; ?>
                <!-- gallery -->
                <?php $tour_gals=get_field('gallery');if($tour_gals) {?>
                    <div class="tour_gallery">
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Gallery</h3>
                                <?php 
                                    $j=1;
                                    $wrap_div1='<div class="row">';
                                    if ($tour_gals): 
                                         
                                        echo '<div class="row">'; 
                                        foreach($tour_gals as $tour_gal):                               
                                            $gal_imgs=aq_resize($tour_gal["url"],390,264,true,true,true);
                                ?>                                                                                          
                                        <div class="col-md-4 col-sm-4">
                                            <div class="gallery-item">
                                                <a href="<?php echo $tour_gal["url"]; ?>" rel="prettyPhoto[gal]" class="prettyPhoto">
                                                    <img src="<?php echo $gal_imgs;?>" alt="<?php echo $tour_gal['title']?>" class="img-responsive">                                                   
                                                </a>                           
                                            </div>
                                        </div>    

                                <?php 
                                        if ($j % 3 === 0 ) { echo '</div>' . $wrap_div1; }
                                        $j++;
                                        endforeach;                        
                                        echo '</div>';  
                                    endif;
                                ?>
                            </div> 
                        </div>
                    </div>
                    <?php }?>

            </div>
            <?php get_sidebar();?>
        </div>  
        
        
    </div><!-- .content-area -->
<?php get_footer(); ?>
