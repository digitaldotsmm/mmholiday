<?php 
/**
 * Template Name: Contact Us
**/ 
get_header(); ?>
<?php GLOBAL $THEME_OPTIONS; ?>
	<div id="primary" class="content-area">
		
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
				<h3 class="widget-title">Contact Info</h3>
				<ul class="contact-info">
				 	<li><i class="fa fa-map-marker"></i><span class="ct-info"><?php echo $THEME_OPTIONS['info_address'];?></span></li>
				 	<li><i class="fa fa-phone"></i><span class="ct-info"><?php echo $THEME_OPTIONS['info_phone'];?></span></li>
				 	<li><i class="fa fa-envelope"></i><a href="mailto:<?php echo $THEME_OPTIONS['info_email'];?>"><span class="ct-info"><?php echo $THEME_OPTIONS['info_email'];?></span></a></li>
				 	<li><i class="fa fa-dribbble"></i><a href="/"><span class="ct-info"><?php echo $_SERVER['HTTP_HOST'];?></span></a></li>
				</ul>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 contactform">
			<h3 class="widget-title">Send us a Message</h3>
				<?php echo do_shortcode('[contact-form-7 id="942" title="Contact Us"]'); ?>
			</div>
		</div>
	</div><!-- .content-area -->
<?php get_footer(); ?>
