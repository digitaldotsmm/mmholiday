<?php

define('TEMPLATE_URL', get_stylesheet_directory_uri());

define('ASSET_URL', TEMPLATE_URL . '/assets/');

require_once('includes/aq_resizer.php');
// Register custom navigation walker
require_once('includes/wp_bootstrap_navwalker.php');
// pages
define('MH_GALLERY', 932);
define('MH_ABOUT_US', 931);
define('MH_ABOUT_MM', 521);
define('MH_HOTELS_IN_MM', 936);
define('MH_CAR_RENTAL', 5524);
define('MH_AIR_TICKETING', 5526);
define('MH_RESERVATION', 581);
define('MH_INTERNATIONAL_AIRLINES', 5681);
define('MH_DOMENSTIC_AIRLINES', 5682);
// post type 
define(MH_TOUR_PROGRAM, 'tour-program'); 
define(MH_DESTINATION, 'destination' );
define(MH_HOTEL, 'hotel');
define(MH_TESTIMONIAL, 'testimonial');

// taxonomy and term
define(MH_HOTEL_LOCATION_TAXO, 'location');
define(MH_TOUR_TYPE_TAXO, 'tour-type');
define(MH_INBOUND_TOUR_TYPE_TAXO, 'inbound-tours');
define(MH_OUTBOUND_TOUR_TYPE_TAXO, 'outbound-tours');
#######################################################

/* * ********* CSS merge and minify process *********** */

#######################################################



if ($_SERVER['HTTP_HOST'] == 'mmholiday.dd' || strpos($_SERVER['HTTP_HOST'], '192') !== false) {

    require( 'includes/class.magic-min.php' );

    $minified = new Minifier(

            array(

        'echo' => false

            )

    );

    //exclude example

    $css_exclude = array(

            //TEMPLATEPATH . '/assets/css/bootstrap.css', 

            //TEMPLATEPATH . '/assets/css/flexslider.css'

    );

    //order example

    $css_order = array(

            //TEMPLATEPATH . '/assets/css/reset.css',     

    );



    $minified->merge(TEMPLATEPATH . '/assets/css/combine.min.css', TEMPLATEPATH . '/assets/css/combine', 'css', $css_exclude, $css_order);

}



function the_template_url() {

    echo TEMPLATE_URL;

}



################################################################################

// Enqueue Scripts

################################################################################



function init_scripts() {

    wp_deregister_script('wp-embed');

    wp_deregister_script('jquery');

    wp_deregister_script('comment-reply');

}



function add_scripts() {

    $js_path = ASSET_URL . 'js';

    $css_path = ASSET_URL . 'css';

    if ($_SERVER['HTTP_HOST'] == 'mmholiday.dd' || strpos($_SERVER['HTTP_HOST'], '192') !== false) {

        $js_libs = array(

            array(

                'name' => 'jquery',

                'src' => $js_path . '/jquery.js',

                'dep' => null,

                'ver' => null,

                'is_footer' => false

            ),

            array(

                'name' => 'bootstrap',
                'src' => $js_path . '/bootstrap.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'responsive-tabs',
                'src' => $js_path . '/responsive-tabs.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            
            // array(

            //     'name' => 'jquery.matchHeight',

            //     'src' => $js_path . '/jquery.matchHeight.js',

            //     'dep' => 'jquery',

            //     'ver' => null,

            //     'is_footer' => true

            // ),
            // array(
            //     'name' => 'jquery.prettyphoto',
            //     'src' => $js_path . '/jquery.prettyphoto.js',
            //     'dep' => 'jquery',
            //     'ver' => null,
            //     'is_footer' => true
            // ), 
            // array(
            //     'name' => 'mydatepicker',
            //     'src' => $js_path . '/jquery.ui.datepicker.min.js',
            //     'dep' => 'jquery',
            //     'ver' => null,
            //     'is_footer' => true
            // ),
            // array(

            //     'name' => 'flexslider',

            //     'src' => $js_path . '/jquery.flexslider.js',

            //     'dep' => 'jquery',

            //     'ver' => null,

            //     'is_footer' => true

            // ),
            array(

                'name' => 'plugins',

                'src' => $js_path . '/plugins.js',

                'dep' => 'jquery',

                'ver' => null,

                'is_footer' => true

            ),
            array(

                'name' => 'script',

                'src' => $js_path . '/script.js',

                'dep' => 'jquery',

                'ver' => null,

                'is_footer' => true

            ),
            

        );



        $css_libs = array(          
            array(
                'name' => 'google-font',
                'src' => '//fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700|Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ), 

            array(

                'name'  => 'bootstrap',

                'src'   => $css_path . '/bootstrap.min.css',

                'dep'   => null,

                'ver'   => null,

                'media' => 'screen'

            ),

            array(

                'name' => 'font-awesomes',

                'src' => $css_path . '/font-awesome.min.css',

                'dep'   => null,

                'ver'   => null,

                'media' => 'screen'

            ),
            array(

                'name' => 'style',

                'src' => TEMPLATE_URL . '/style.css',

                'dep'   => null,

                'ver'   => ASSET_VERSION,

                'media' => 'screen'

            ),
			
			// array(

   //              'name' => 'effects',

   //              'src' => $css_path . '/effects.css',

   //              'dep'   => null,

   //              'ver'   => ASSET_VERSION,

   //              'media' => 'screen'

   //          ),
   // //          array(

   //              'name' => 'responsive',

   //              'src' => $css_path . '/responsive.css',

   //              'dep'   => null,

   //              'ver'   => ASSET_VERSION,

   //              'media' => 'screen'

   //          ),
            

        );

    } else {

        $js_libs = array(

            array(

                'name' => 'jquery',

                'src' => 'https://cdn.jsdelivr.net/jquery/2.2.4/jquery.js',

                'dep' => null,

                'ver' => null,

                'is_footer' => false

            ),

            array(

                'name' => 'bootstrap',

                'src' => 'https://cdn.jsdelivr.net/bootstrap/3.3.7/js/bootstrap.min.js',

                'dep' => 'jquery',

                'ver' => null,

                'is_footer' => true

            ),
            array(

                'name' => 'responsive-tabs',

                'src' => 'https://cdn.jsdelivr.net/bootstrap.responsive-tabs/0.0.1/responsive-tabs.js',

                'dep' => 'jquery',

                'ver' => null,

                'is_footer' => true

            ),
            
            // array(

            //     'name' => 'jquery.matchHeight',

            //     'src' => $js_path . '/jquery.matchHeight.js',

            //     'dep' => 'jquery',

            //     'ver' => null,

            //     'is_footer' => true

            // ),
            // array(
            //     'name' => 'jquery.prettyphoto',
            //     'src' => $js_path . '/jquery.prettyphoto.js',
            //     'dep' => 'jquery',
            //     'ver' => null,
            //     'is_footer' => true
            // ), 
            // array(
            //     'name' => 'mydatepicker',
            //     'src' => $js_path . '/jquery.ui.datepicker.min.js',
            //     'dep' => 'jquery',
            //     'ver' => null,
            //     'is_footer' => true
            // ),
            // array(

            //     'name' => 'flexslider',

            //     'src' => $js_path . '/jquery.flexslider.js',

            //     'dep' => 'jquery',

            //     'ver' => null,

            //     'is_footer' => true

            // ),
            array(

                'name' => 'plugins',

                'src' => $js_path . '/plugins.js',

                'dep' => 'jquery',

                'ver' => null,

                'is_footer' => true

            ),
            array(

                'name' => 'script',

                'src' => $js_path . '/script.js',

                'dep' => 'jquery',

                'ver' => null,

                'is_footer' => true

            ),

        );



        $css_libs = array(

            array(
                'name' => 'google-font',
                'src' => '//fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700|Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ), 

            array(

                'name'  => 'bootstrap',

                'src'   => 'https://cdn.jsdelivr.net/bootstrap/3.3.7/css/bootstrap.min.css',

                'dep'   => null,

                'ver'   => null,

                'media' => 'screen'

            ),

            array(

                'name' => 'font-awesomes',

                'src' =>  'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css',

                'dep'   => null,

                'ver'   => null,

                'media' => 'screen'

            ),
            array(

                'name' => 'style',

                'src' => TEMPLATE_URL . '/style.css',

                'dep'   => null,

                'ver'   => ASSET_VERSION,

                'media' => 'screen'

            ),

        );

    }

    foreach ($js_libs as $lib) {

        wp_enqueue_script($lib['name'], $lib['src'], $lib['dep'], $lib['ver'], $lib['is_footer']);

    }

    foreach ($css_libs as $lib) {

        wp_enqueue_style($lib['name'], $lib['src'], $lib['dep'], $lib['ver'], $lib['media']);

    }

}



function my_deregister_scripts() {

    global $post_type;



    if (!is_front_page() && !is_home()) {

        

    }

    if (!is_page('contact-us')) { //only include in contact us page

    }

}



function my_deregister_styles() {

    global $post_type;

    if (!is_page('contact-us')) { //only include in contact us page

    }

}



if (!is_admin())

    add_action('init', 'init_scripts', 10);

add_action('wp_enqueue_scripts', 'add_scripts', 10);

add_action('wp_enqueue_scripts', 'my_deregister_scripts', 100);

add_action('wp_enqueue_scripts', 'my_deregister_styles', 100);







################################################################################

// Add theme support

################################################################################



add_theme_support('automatic-feed-links');

add_theme_support('nav-menus');

add_post_type_support('page', 'excerpt');

add_theme_support('post-thumbnails', array('post', 'page',MH_DESTINATION, MH_TOUR_PROGRAM));



if (function_exists('register_nav_menus')) {

    register_nav_menus(

            array(

                'main' => 'Main',

                'helpful' => 'Helpful Info',

                'privacy' => 'Privacy Info',

            )

    );

}



################################################################################

// Add theme sidebars

################################################################################



if (function_exists('register_sidebar')) {

    register_sidebar(array(

        'name' => __('Main - Sidebar'),

        'id' => 'main-sidebar-widget-area',

        'description' => 'Widgets in this area will be shown on the right sidebar of default page',

        'before_widget' => '<aside class="widget">',

        'after_widget' => '</aside>',

        'before_title' => '<h3 class="widget-title">',

        'after_title' => '</h3>',

    ));
    register_sidebar(array(

        'name' => __('Tour Packages Widget'),

        'id' => 'tour_sidebar-widget_area',

        'description' => 'Widgets in this area will be shown on the right sidebar of tour page',

        'before_widget' => '<aside class="widget">',

        'after_widget' => '</aside>',

        'before_title' => '',

        'after_title' => '',

    ));
    register_sidebar(array(

        'name' => __('Destinations Widget'),

        'id' => 'destinations-widget_area',

        'description' => 'Widgets in this area will be shown on the right sidebar of destination page',

        'before_widget' => '<aside class="widget">',

        'after_widget' => '</aside>',

        'before_title' => '',

        'after_title' => '',

    ));
    register_sidebar(array(

        'name' => __('Footer 1'),

        'id' => 'footer-1',

        'description' => 'Widgets in this area will be shown on the right sidebar of default page',

        'before_widget' => '<aside class="widget">',

        'after_widget' => '</aside>',

        'before_title' => '<h3 class="footer-title">',

        'after_title' => '</h3>',

    ));
    register_sidebar(array(

        'name' => __('Footer 2'),

        'id' => 'footer-2',

        'description' => 'Widgets in this area will be shown on the right sidebar of default page',

        'before_widget' => '<aside class="widget">',

        'after_widget' => '</aside>',

        'before_title' => '',

        'after_title' => '',

    ));
    register_sidebar(array(

        'name' => __('Footer 3'),

        'id' => 'footer-3',

        'description' => 'Widgets in this area will be shown on the right sidebar of default page',

        'before_widget' => '<aside class="widget">',

        'after_widget' => '</aside>',

        'before_title' => '<h3 class="footer-title">',

        'after_title' => '</h3>',

    ));
//    register_sidebar(array(

//        'name' => __('Page - Sidebar'),

//        'id' => 'page-widget-area',

//        'description' => 'Widgets in this area will be shown on the right sidebar of pages',

//        'before_widget' => '<aside class="widget">',

//        'after_widget' => '</aside>',

//        'before_title' => '',

//        'after_title' => '',

//    ));

//    register_sidebar(array(

//        'name' => __('Footer Newsletter - Sidebar'),

//        'id' => 'footer-newsletter-widget-area',

//        'description' => 'Widgets in this area will be shown on the top of footer',

//        'before_widget' => '<div class="row" id="newsletter-form-container">',

//        'after_widget' => '</div>',

//        'before_title' => '',

//        'after_title' => '',

//    ));

//    register_sidebar(array(

//        'name' => __('Footer Left - Sidebar'),

//        'id' => 'footer-left-widget-area',

//        'description' => 'Widgets in this area will be shown on the left-hand side of footer',

//        'before_widget' => '',

//        'after_widget' => '',

//        'before_title' => '',

//        'after_title' => '',

//    ));

//    register_sidebar(array(

//        'name' => __('Footer Middle - Sidebar'),

//        'id' => 'footer-middle-widget-area',

//        'description' => 'Widgets in this area will be shown in the middle of footer',

//        'before_widget' => '',

//        'after_widget' => '',

//        'before_title' => '',

//        'after_title' => '',

//    ));

//    register_sidebar(array(

//        'name' => __('Footer Right - Sidebar'),

//        'id' => 'footer-right-widget-area',

//        'description' => 'Widgets in this area will be shown on the right-hand side of footer',

//        'before_widget' => '',

//        'after_widget' => '',

//        'before_title' => '',

//        'after_title' => '',

//    ));

}






################################################################################
// Pagination
################################################################################
if ( ! function_exists( 'dd_pagination' ) ) :
    function dd_pagination() {
        global $wp_query;

        $big = 999999999; // need an unlikely integer
        
        echo paginate_links( array(
            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $wp_query->max_num_pages,
                        'show_all' => false,
                        'end_size' => 1,
                        'mid_size' => 2,
                        'prev_next' => true,
                        'prev_text' => __('First'),
                        'next_text' => __('Last'),
                ) );
    }
endif;


//post per page
function progress_archive( $query ) {

    if( $query->is_main_query() && !is_admin() && is_post_type_archive( MH_DESTINATION ) ) {
        $query->set( 'posts_per_page', '12' );
    }
    if( $query->is_main_query() && !is_admin() &&  is_post_type_archive( MH_TOUR_PROGRAM ) || is_tax() ) {
        $query->set( 'posts_per_page', '12' );
    }
    
}
add_action( 'pre_get_posts', 'progress_archive' ); 


################################################################################

// Comment formatting

################################################################################



function theme_comments($comment, $args, $depth) {

    $GLOBALS['comment'] = $comment;

    ?>

    <li>

        <article <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">

            <header class="comment-author vcard">

    <?php echo get_avatar($comment, $size = '48', $default = '<path_to_url>'); ?>

    <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>

                <time><a href="<?php echo htmlspecialchars(get_comment_link($comment->comment_ID)) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time()) ?></a></time>

                <?php edit_comment_link(__('(Edit)'), '  ', '') ?>

            </header>

                <?php if ($comment->comment_approved == '0') : ?>

                <em><?php _e('Your comment is awaiting moderation.') ?></em>

                <br />

            <?php endif; ?>



    <?php comment_text() ?>



            <nav>

            <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>

            </nav>

        </article>

        <!-- </li> is added by wordpress automatically -->

                <?php

            }

// function custom_shortcode($atts) {
//     $args = array(  
//     'tour-type' => 'inbound-tours',
//     'post_type' => 'tour-program',
//     'posts_per_page' => 3,
//     'orderby' => 'date',
//     'order' => 'DESC',
//     );
//     $list ="";
//     $recent_posts = wp_get_recent_posts( $args );
//     $list .= '<div class="row inbound-tours-list">';
//     foreach( $recent_posts as $recent ){
//         //print_r($recent);
        
//         $list .= '<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"><div class="inbound-tours-item">';
//             $list .= '<div class="tours-image-wrapper hover ehover6" aria-haspopup="true">
//                         <a href="' . get_permalink($recent["ID"]) . '"><img src="'. get_field('featured_image', $recent["ID"]) .'" /></a>
//                       </div>';
                        
//             $list .= '<div class="tour-des">';
// 				$list .= '<div class="triangle-bottomleft"></div>';
// 				$list .= '<div class="tours-content-wrapper">';
// 					$list .= '<div class="tours-content-left"></div>';
// 					$list .= '<div class="tours-content-right">';
// 						$list .= '<h2 class="entry-title" data-fontsize="18" data-lineheight="27"><a href="' . get_permalink($recent["ID"]) . '">'.$recent["post_title"].'</a></h2>';       
// 						$list .= '<div class="tours-duration">'. get_field('duration', $recent["ID"]) .'</div>';
// 						$list .= '<div class="tours-excerpt">'. $recent["post_excerpt"] .'</div>';      
// 					$list .= '</div>';
// 				$list .= '</div>';
//             $list .= '</div>';
        
//         $list .= '</div></div>';
// 	}
//     $list .= '</div>';
//     wp_reset_query();
//     return $list ;  
// }
// add_shortcode( 'inbound-tours', 'custom_shortcode' ); 
// function custom_shortcode2($atts) {
//     $args = array(  
//     'post_type' => 'destination',
//     'posts_per_page' => 20,
//     'orderby' => 'date',
//     'order' => 'DESC',
//     );
//     $list ="";
//     $recent_posts = wp_get_recent_posts( $args );
//     $list .= '<div class="flexslider"><ul class="slides">';
//     foreach( $recent_posts as $recent ){
//         //print_r($recent);
        
//         $list .= '<li>';
//             $list .= '<div class="destination-image-wrapper" aria-haspopup="true">
//                         <a href="' . get_permalink($recent["ID"]) . '"><img src="'. get_field('featured_image2', $recent["ID"]) .'" /></a>
//                       </div>';      
//                     $list .= '<h4 class="entry-title" data-fontsize="18" data-lineheight="27"><a href="' . get_permalink($recent["ID"]) . '">'.$recent["post_title"].'</a></h4>';                       
//         $list .= '</li>';
// 	}
//     $list .= '</ul></div>';
//     wp_reset_query();
//     return $list ;  
// }
// add_shortcode( 'destination', 'custom_shortcode2' );     

function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}  

function ajax_sendmail_func(){
    
    GLOBAL $THEME_OPTIONS;                         
        
        if( is_array($_POST['lang_require']) ){
            $languages = implode(',', $_POST['lang_require']);
        }else{
            $languages = $_POST['lang_require'];
        }
        
        
        $customize_body = "Tour Program Name : " . $_POST['tour_title'] . "<br/>
                   Arrival Date  : ". $_POST['arrivaldate'] . "<br/>
                   Depature Date  : ". $_POST['depaturedate'] . "<br/>
                   Other Interested Places : ". $_POST['interested_places'] . "<br/>
                   Hotel Accommodation  : ". $_POST['hotel_accommodation'] ."<br/>
                   Hotel Type  : ". $_POST['hotel_star'] . " star<br/>
                   Target Rate : ". $_POST['target_rate'] . " US$ <br/>
                   Flight to and from Destination(s)  : ". $_POST['flight_des'] ."<br/>
                   Tour Guide  : ". $_POST['tour_guide'] . " ( " . $_POST['guide_type'] . " ) <br/>
                   Language  : ". $languages ."<br/><br/>";
    
        $body = "Tour Reservation Form <br/><br/>
                Tour Information<br/>" . $customize_body ."
                Personal Information<br/>
                Name  : ". $_POST['title'] . " ". $_POST['contactName'] . "<br/>
                Email  : " . $_POST['email'] . "<br/>
                Phone  : " . $_POST['phone'] . "<br/>
                Nationality  : ". $_POST['nationality'] . "<br/>
                Sex  : " . $_POST['sex'] . "<br/>
                Address  : " . $_POST['address'] . "<br/>
                Passport No  : ". $_POST['passportno'] . "<br/>
                Adult  : " . $_POST['adult'] . "<br/>
                Child  : ". $_POST['child'] . "<br/>
                Infants  : ". $_POST['infants'] . "<br/>
                Additional Message  : ". $_POST['add_msg'] . "<br/>";
                $subject = "Tour Reservation";
        $to = $THEME_OPTIONS['reservation_form_email'];
                 
                $content .= "<html>\n";
                $content .= "<body style=\"font-family:Verdana, Verdana, Geneva, sans-serif; font-size:12px; color:#666666;\">\n";
                $body    = html_entity_decode($body);
                $content .= $body;
                $content .= "\n\n";
                $content .= "</body>\n";
                $content .= "</html>\n";

                $headers[] = 'From: Myanmar Holiday Travel and Tour <noreply@mmholiday.com>';
                $headers[] = "MIME-Version: 1.0\n";
                $headers[] = "Content-type: text/html; charset=UTF-8\r\n";            


                if( !$to ){  // use admin mail if there is no reservation form mail
                    $to = get_option('admin_email');
                }else{
                    if( strpos($to, ',') ){ // if it is comman separated, changed to array
                        $to = explode(',', str_replace(' ', '', $to));
                    }
                }                  
//                echo $to, $subject, $content, $headers;           
               //if( 1 == 1 ){                    
                if(wp_mail($to, $subject, $content, $headers)){
                    $data['status']= 'yes';
                }else{
                    $data['status']= 'no';
                }
                echo json_encode($data);
                die;
                
}

add_action('wp_ajax_sendmail', 'ajax_sendmail_func');           // for logged in user  
add_action('wp_ajax_nopriv_sendmail', 'ajax_sendmail_func');

//ajax captcha check
function ajax_check_captcha() {

    $data = array();

    if (empty($_SESSION['captcha']) || trim(strtolower($_REQUEST['value'])) != $_SESSION['captcha']) {
        $data['status'] = 'error';
        $data['valid'] = 'no';
    } else {
        $data['status'] = 'success';
        $data['valid'] = 'yes';
    }
    
    echo json_encode($data);    
    die;
}