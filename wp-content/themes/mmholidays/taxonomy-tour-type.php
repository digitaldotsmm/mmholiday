<?php get_header(); ?>
	<div id="primary" class="content-area">
		<div class="row">
			<div class="col-md-12 ">
				<div class="inbound-tours-list">
					<?php 
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						if(have_posts()):
			            while ( have_posts() ) : the_post();
			            	$img2=get_attachment_image_src($post->ID, 'full');
			        		$tour_img=aq_resize($img2[0],361,350,true,true,true);
	                ?>
	                		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
	                            <div class="inbound-tours-item">
	                                <div class="tours-image-wrapper hover ehover6" aria-haspopup="true">
	                                    <a title="thumbnail" href="<?php the_permalink(); ?>"> <img src="<?php echo $tour_img; ?>" /></a>
	                                </div>
									<div class="tour-des">
										<div class="triangle-bottomleft"></div>
										<div class="tours-content-wrapper">
											<div class="tours-content-left"></div>
											<div class="tours-content-right">
												<h2 class="entry-title" data-fontsize="18" data-lineheight="27"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
												<div class="tours-duration"><?php echo get_field('tour_duration');?></div>
												<div class="tours-excerpt"><?php echo wp_trim_words( get_the_excerpt(), 12 ); ?></div>
											</div>
											
										 </div>       
	                                 </div>       
	                            </div>
	                            
	                        </div>
	                <?php
			           	endwhile;
			        ?>
			        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
				        <nav aria-label="Page navigation">
				            <ul class="pagination">
				                <?php dd_pagination(); ?>
				            </ul>
				        </nav>
				    </div>	
				    <?php endif; wp_reset_query();  ?>
		        </div>
			</div>			
		</div>	
		
        
	</div><!-- .content-area -->
<?php get_footer(); ?>
