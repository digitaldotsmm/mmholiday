            <div id="footer">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <?php
            				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer 1')):
            				endif;
        				?>
                    </div>
                    
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 contact-info">
                        <?php
            				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer 2')):
            				endif;
        				?>
                    </div>
                    
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 gallery">
                        <?php
            				if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer 3')):
            				endif;
        				?>
                    </div>
                    
                </div>
            </div>
            <div class="footer-copyright">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <p style="text-align: left;">Copyright &COPY; <?php echo date('Y')?> All Rights Reserved</p>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <p  style="text-align: right;">Created by <a href="http://www.digitaldots.com.mm" target="_blank" title="Web Development in Yangon: Digital Dots"><img src="<?php echo ASSET_URL;?>images/digital-dots-logo.png" alt="Web Development in Yangon: Digital Dots" /></a></p>
                    </div>
                </div>
            </div>      
        </div>
    </div>
<?php wp_footer(); ?>
</body>
</html>