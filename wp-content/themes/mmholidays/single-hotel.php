<?php get_header(); ?>	
<div id="primary" class="content-area">	
    <div class="row">
        <?php  
            while ( have_posts() ) : the_post(); ?>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div style="margin-bottom: 25px;" class="single-image hover ehover11">
                        <img src="<?php echo get_field('image_1'); ?>" />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                    <div class="single-content">
                        <ul>
                            <li>
                                <label>Room Rate:</label><br />
                                <span><?php echo get_field('room_rate'); ?></span>
                            </li>
                            <li>
                                <label>Address:</label><br />
                                <span><?php echo get_field('address');?></span>
                            </li>
							<li>
                                <label>Phone:</label><br />
                                <span><?php echo get_field('phone');?></span>
                            </li>
                            <li>
                                <label>Website:</label><br />
                                <span><a target="_blank" href="<?php echo addhttp(get_field('website'));?>"><?php echo addhttp(get_field('website'));?></a></span>
                            </li>
                        </ul>
                       
                    </div>    
                </div>  
        <?php endwhile; ?>	
    </div>
    <div class="repeate-detail">
        <h2>Detail</h2>
        <?php echo the_content(); ?>
    </div>
</div><!-- .content-area -->
<?php get_footer(); ?>