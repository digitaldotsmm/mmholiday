<?php global $THEME_OPTIONS; ?>

<!doctype html>

<!--[if lt IE 7 ]>	<html lang="en" class="no-js ie6"> <![endif]-->

<!--[if IE 7 ]>		<html lang="en" class="no-js ie7"> <![endif]-->

<!--[if IE 8 ]>		<html lang="en" class="no-js ie8"> <![endif]-->

<!--[if IE 9 ]>		<html lang="en" class="no-js ie9"> <![endif]-->

<!--[if (gt IE 9)|!(IE)]><!-->

<html dir="ltr" lang="en"  class="no-js">

<!--<![endif]-->

<head>

<meta charset="UTF-8">

<title><?php wp_title()?></title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />

<?php if ( file_exists(TEMPLATEPATH .'/favicon.png') ) : ?>

<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.png">
<?php endif; ?>
<!--[if lt IE 9]>

<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>

<![endif]-->

<?php wp_head(); ?>
</head>

<?php $body_classes = join( ' ', get_body_class() ); ?>

<body class="<?php if( !is_search() )echo $body_classes; ?>">
    <div class="container">
        <div id="main">
            <div id="header">
                <div class="row header-top">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
                        <div class="logo">
                            <?php $header_logo=$THEME_OPTIONS['logo'];if($header_logo){$logo=$header_logo;}else { $logo=ASSET_URL.'images/header-logo.png';} ?>
                            <a href="<?php echo home_url(); ?>"><img alt="mmholidays" src="<?php echo $logo; ?>" /></a>
                        </div>
                        <div class="slogan"><h3><?php echo $THEME_OPTIONS['slogan']; ?></h3></div>
                    </div>
                    <!-- <div class="col-xs-12 col-sm-8 col-md-3 col-lg-4">   -->
                        <!-- <div class="slogan"><h3><?php //echo $THEME_OPTIONS['slogan']; ?></h3></div> -->
                    <!-- </div> -->
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
                        <div class="info-right alignright">
                            <div id="flags-wrapper">
                                <?php echo do_shortcode('[google-translator]'); ?>
                            </div>  
                            <div class="info_socal">
                                <ul class="info">
                                    <li><span class="fa fa-phone" aria-hidden="true"></span><?php echo $THEME_OPTIONS['info_hotline'];?></li>
                                    <li><a href="mailto:<?php echo $THEME_OPTIONS['info_email'];?>"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><?php echo $THEME_OPTIONS['info_email'];?> </a></li>
                                </ul>
                                 
                                <ul class="social">
                                    <li class="header_search"><?php get_search_form(); ?></li>
                                    <?php if($THEME_OPTIONS['facebookid']) { ?><li><a href="<?php echo $THEME_OPTIONS['facebookid'];?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li><?php }?>
                                    <?php if($THEME_OPTIONS['twitterid']) { ?><li><a href="<?php echo $THEME_OPTIONS['twitterid'];?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li><?php } ?>
                                    <?php if($THEME_OPTIONS['googleplus']) { ?><li><a href="<?php echo $THEME_OPTIONS['googleplus'];?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li><?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div><!-- end header top -->
                <div class="row header-menu">
                    <div class="col-lg-12">
                        <nav id="navbar-primary" class="navbar navbar-default" role="navigation">
                              <!-- <div class="container-fluid"> -->
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-primary-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                  </button>
                                </div>
                                <div class="collapse navbar-collapse" id="navbar-primary-collapse">
                                  <?php wp_nav_menu( array( 'theme_location' => 'main','menu_class' => 'nav navbar-nav','walker' => new wp_bootstrap_navwalker() ) ); ?> 
                                </div><!-- /.navbar-collapse -->
                              <!--</div><!-- /.container-fluid -->
                        </nav>
                         
                    </div>
                </div><!-- end header top -->
            </div>

            <?php if(!is_front_page() && !is_home()) { ?>
            <div class="header-banner">
                <?php                     
                    // get current quried object
                    $quried_obj = get_queried_object();                                
                    // if is archive page
                    if( is_archive() ){  
                        // if is taxonomy archive
                        if( is_tax() ){                    
                            $head_title=ucfirst($quried_obj->name);
                        }else{ // if is normal archive
                            $head_title=ucfirst($quried_obj->labels->name);
                        }
                    }                               
                    elseif(is_singular()){ // if is single page
                        $head_title=get_the_title();
                    } 
					elseif(is_404()){
						$head_title="Something is wrong! Please find other.";
					}
					else {
						$head_title=get_the_title();
					}
						
                ?>
                <?php 
                    $header_banner=get_field('header_banner',$post->ID,true);
                    if($header_banner) {
                        $header_img=$header_banner;
                    }
                    elseif(is_singular(MH_TOUR_PROGRAM) || is_singular(MH_DESTINATION)) {
                        $tour_img= get_attachment_image_src($post->ID, 'full');
                        $header_img= aq_resize($tour_img[0],1112,300,true,true,true);
                    }
                    else {
                        $header_img=ASSET_URL.'images/slider.jpg';
                    }

                    
                    if(is_page_template('template-contact.php')) {
                ?>
                        <iframe src="https://www.google.com/maps/d/embed?mid=1bpcTvO_BR_kmkC88orSGuKGWE4g&amp;z=13" width="100%" height="480"></iframe>
                <?php        
                    }else {         
                ?>
            
                        <div class="banner-image" style="background-image: url('<?php echo $header_img;?>')">
                            <div class="header-section_title">
                                <h1><?php echo $head_title;?></h1>
                            </div>
                        </div>
                <?php }?>
            </div>
            <?php } ?>

	