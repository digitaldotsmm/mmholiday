<?php
/**
 * Template Name: Destinations
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
get_header(); ?>
	<div id="primary" class="content-area">
		<div class="row">
			<div class="col-md-12 destination-list">
                <?php
					if ( have_posts() ) :
						$counter = 0;
							echo  '<div class="row">';
	                	while ( have_posts() ): the_post();
							if(($counter % 3 ==0) && ($counter > 0)){
								echo '</div><div class="row">';
							 }
				?>
                    		<div style="margin-bottom: 30px;" class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                <div class="destination-item">
                                    <div class="destination-image-wrapper" aria-haspopup="true">
                                        <a title="thumbnail" href="<?php the_permalink(); ?>"> <img src="<?php echo get_field('featured_image2'); ?>" /></a>
                                    </div>
                                    <div class="destination-content-wrapper">
                                        <h2 class="entry-title" data-fontsize="18" data-lineheight="27"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                        <div class="tours-excerpt"><?php echo wp_trim_words( get_the_content(), 15 ); ?></div>                                          
                                     </div>       
                                </div>
                                
                            </div>
                <?php $counter++; endwhile; endif; wp_reset_query();  ?>
			</div>
		</div>	        
	</div>
<?php get_footer(); ?>