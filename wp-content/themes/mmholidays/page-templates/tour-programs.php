<?php
/**
 * Template Name: Tour Programs
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="main-content" class="main-content">

<?php
	if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>
    <div class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</div><!-- .entry-header -->
    
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
					the_content(); ?>
                <?php
                $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
				$query = new WP_Query( array( 
					'orderby' => 'date',
					'order' => 'DESC' 
				  ) ); 

				 //set our query's pagination to $paged
				 $query -> query('post_type=tour-program&posts_per_page=9'.'&paged='.$paged);
                /* $args = array(
                    'posts_per_page' => 9,
                    'post_type' => 'tour-program',
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'paged' => $paged
                );
                query_posts( $args ); */
                ?>
                <div class="inbound-tours-list">
                        <?php
                        if ( $query->have_posts() ) :
							$counter = 0;
							//echo  '<div class="row">';
                        	while ( $query->have_posts() ): $query->the_post(); 
								/* if(($counter % 3 ==0) && ($counter > 0)){
									echo '</div><div class="row">';
								 } */
							
							?>
                            <?php// print_r($recent); ?>
                        		<div style="margin-bottom: 30px;" class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                                    <div class="inbound-tours-item">
                                        <div class="tours-image-wrapper hover ehover6" aria-haspopup="true">
                                            <a title="thumbnail" href="<?php the_permalink(); ?>"> <img src="<?php echo get_field('featured_image'); ?>" /></a>
                                        </div>
										<div class="tour-des">
											<div class="triangle-bottomleft"></div>
											<div class="tours-content-wrapper">
												<div class="tours-content-left"></div>
												<div class="tours-content-right">
													<h2 class="entry-title" data-fontsize="18" data-lineheight="27"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
													<div class="tours-duration"><?php echo get_field('duration');?></div>
													<div class="tours-excerpt"><?php echo wp_trim_words( get_the_excerpt(), 12 ); ?></div>
												</div>
												
											 </div>       
                                         </div>       
                                    </div>
                                    
                                </div>
                        <?php
								//$counter++;
							endwhile;
                        ?>
							</div>
							<div class="row pagenav">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<div class="alignleft"><?php previous_posts_link('Previous', $query->max_num_pages) ?></div>
									<div class="alignright"><?php next_posts_link('Next', $query->max_num_pages) ?></div>
								</div>
							</div>
						<?php	
                        endif;   
						wp_reset_query();   
                    ?>
                    
                
            </div>
				<?php
				endwhile; 
			?>
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();
