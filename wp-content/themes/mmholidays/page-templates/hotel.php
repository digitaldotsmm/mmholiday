<?php
/**
 * Template Name: Hotel
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
		<div class="row">
			<div class="col-md-12">
				<div id="tours-tabs">
                    <ul class="nav nav-tabs responsive">  
                    	<?php 
                            $hloactions=  get_terms(MH_HOTEL_LOCATION_TAXO,'hide_empty=0');
                            if($hloactions): foreach ($hloactions as $skey=>$hloaction):
                                $term_link= get_term_link($hloaction); 
                        ?>
                                <li><a data-toggle="tab" href="#<?php echo $hloaction->slug;?>"><?php echo $hloaction->name;?></a></li>   
                        <?php           
                            endforeach;endif;
                        ?>
                    </ul>
	                <div class="tab-content responsive"> 
	                	<?php          
                            if($hloactions): foreach ($hloactions as $hkey=>$hloaction):
                        ?>
                    	<div id="<?php echo $hloaction->slug;?>" class="tab-pane fade <?php echo ($hkey==0)?'in':'';?> pd15">
                    		<div class="row ">
                		<?php
                                    $hotel_arg = array('post_type' => MH_HOTEL, 'posts_per_page' => -1, 'tax_query' => array(array('taxonomy' => MH_HOTEL_LOCATION_TAXO, 'field' => 'slug', 'terms' => $hloaction->slug)));
                                    $hotel_qry = new WP_Query($hotel_arg);
                                    if ($hotel_qry->have_posts()): $count = 1; while ($hotel_qry->have_posts()): $hotel_qry->the_post();    
	                                    $img_url=  get_field('image_1',get_the_ID(),true);
	                                    $hotelimg=  aq_resize($img_url,600,400,true,true,true);
	                                   	// if($hotelimg){
                        ?>
	                                    <div class="col-md-4 col-sm-4 each_hotel">
	                                        <div class="column_attr">
	                                            <a href="<?php echo $img_url;?>" rel="prettyphoto[fesgal]">
	                                                <img src="<?php echo $hotelimg; ?>" class="hotel_img img-responsive" alt="<?php echo get_the_title(); ?>"/>
	                                            </a>    
	                                            <div class="hotel_datas">
	                                            	<h4><?php echo get_the_title(); ?></h4>
	                                            	<div class="about_fes">
	                                            		<ul>
	                                            		<?php  if(get_field('address')){?><li><i class="fa fa-home"></i><?php  echo get_field('address');?></li><?php }?>                                           		
	                                            		<?php  if(get_field('phone')){?><li><i class="fa fa-phone"></i><?php  echo get_field('phone');?></li><?php }?> 
	                                            		<?php  if(get_field('email')){?><li><i class="fa fa-envelope"></i><?php  echo get_field('email');?></li><?php }?> 
	                                            		<?php  if(get_field('star_rate')){?><li><i class="fa fa-star"></i><?php  echo get_field('star_rate');?></li><?php }?> 
	                                            		<?php  if(get_field('website')){?><li><i class="fa fa-dribbble"></i><?php  echo get_field('website');?></li><?php }?> 
	                                            	</div>  
	                                            </div>                                               	                                                                                             
	                                        </div>
	                                    </div>
                                    <?php //}  // end of has image , if not, not show all info ?>
	                            <?php if( $count % 3 == 0 ){ echo '</div><!-- end .row --><div class="row">'; } ?>
		                        <?php
		                                $count++;     endwhile; endif;
		                        ?>
                        	</div><!-- end .row -->
                        </div>
                        <?php
                            endforeach;endif;
                        ?>
	                </div>
	            </div>
			</div>
		</div>	       
	</div><!-- .content-area -->
<?php 	get_footer(); ?>
