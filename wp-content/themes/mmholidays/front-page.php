<?php get_header(); ?>
	<?php echo do_shortcode('[rev_slider alias="home-slider"]');?>
	<div class="row home-about">
		<div class="home-about-left col-xs-12 col-sm-8 col-md-8 col-lg-8">
			<div class="about-us">			
				<?php $about_page = get_page(MH_ABOUT_US); ?>
				<a href="/"><img class="alignnone " src="<?php echo ASSET_URL.'images/header-logo.png';?>" alt="logo"/></a> <!--  width="179" height="102"  -->
				<h4 class="title-border"><?php echo $about_page->post_title;?></h4>
				<h3>Mingalar Par...!</h3>
				<p><?php echo string_limit_words($about_page->post_content,28); ?></p>
				<a class="read-more" href="<?php echo get_permalink($about_page->ID); ?>">Read More</a>
			</div>
		</div>
		<div class="home-about-right col-xs-12 col-sm-4 col-md-4 col-lg-4">
			<div class="about-myanmar">
				<?php $aboutmm_page = get_page(MH_ABOUT_MM); ?>
				<?php 
                    $img = get_attachment_image_src($aboutmm_page->ID, 'large'); 
                    $mm_img=aq_resize($img[0],136,131,true,true,true);              
                ?>
				<h4 class="title-border"><?php echo $aboutmm_page->post_title;?></h4>
				<div class="aboutmm_img">
					<img class="img_shape" src="<?php echo ASSET_URL.'images/moon-shade.png';?>" alt="image shape"/>
					<img class="alignleft main_img" src="<?php echo $mm_img;?>" alt="about myanmar"/><!-- width="136" height="131" -->
				</div>
				<p><?php echo $aboutmm_page->post_excerpt; ?></p>
				<div class="read-more-2"><a href="<?php echo get_permalink($aboutmm_page->ID); ?>">Read More</a></div>
			</div>
		</div>
	</div>
	<div class="inbound-tours">
		<h4 class="title-border">Inbound Tours</h4>
			<?php 
				$args = array(  
				    'tour-type' => MH_INBOUND_TOUR_TYPE_TAXO,
				    'post_type' => MH_TOUR_PROGRAM,
				    'posts_per_page' => 3,
				    'orderby' => 'date',
				    'order' => 'DESC',
			    );
				    $list ="";
				    $recent_posts = wp_get_recent_posts( $args );
			?>
			<div class="row inbound-tours-list">
				<?php
				    foreach( $recent_posts as $recent ){
				    	$img1=get_attachment_image_src($recent['ID'], 'full');
				    	$inbound_img=aq_resize($img1[0],361,350,true,true,true);
				?>
				    	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
				    		<div class="inbound-tours-item">
					            <div class="tours-image-wrapper hover ehover6" aria-haspopup="true">
			                        <a href="<?php echo get_permalink($recent['ID'])?>"><img src="<?php echo $inbound_img; ?>" /></a>
			                    </div>
					                        
					            <div class="tour-des">
									<div class="triangle-bottomleft"></div>
										<div class="tours-content-wrapper">
											<div class="tours-content-left"></div>
											<div class="tours-content-right">
												<h2 class="entry-title" data-fontsize="18" data-lineheight="27"><a href="<?php echo get_permalink($recent['ID']); ?>"> <?php echo $recent["post_title"] ?></a></h2>     
												<div class="tours-duration"><?php echo get_field('tour_duration', $recent["ID"]); ?></div>
												<!-- <?php //if($recent["post_excerpt"]) { ?><div class="tours-excerpt"><?php //echo $recent["post_excerpt"] ?></div>   <?php// }?>  -->
											</div>
										</div>
				            	</div>				        
				       		</div>
				       </div>
				<?php	} ?>
		    </div>
			
		<div class="read-more-2"><a href="/<?php echo MH_TOUR_TYPE_TAXO;?>/<?php echo MH_INBOUND_TOUR_TYPE_TAXO;?>/">View All</a></div>
	</div>
	<div class="row inbound-tours our-destinations">
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
			<div class="hotels-in-myanmar">
				<?php $hotels_page = get_page(MH_HOTELS_IN_MM); ?>
				<?php 
                    $himg = get_attachment_image_src($hotels_page->ID, 'large'); 
                    $hotel_img=aq_resize($himg[0],318,271,true,true,true);              
                ?>
				<img class="alignleft" src="<?php echo $hotel_img;?>" alt="hotels-in-myanmar"/>				
				<h2><?php echo $hotels_page->post_title;?></h2>
				<div class="mask">
                    <a href="<?php echo get_permalink($hotels_page->ID)?>" class="wrapmask">
                      <span><i class="fa fa-search"></i></span>
                   </a>
                </div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 dests-wrap">
			<h4 class="title-border">Our Destinations</h4>
			<?php
				$args = array(  
				    'post_type' => MH_DESTINATION, 
				    'posts_per_page' => 20,
				    'orderby' => 'date',
				    'order' => 'DESC',
			    );
			    $list ="";
			    $des_posts = wp_get_recent_posts( $args );
		    ?>
			    <div class="flexslider">
			    	<ul class="slides">
				    	<?php 
				    		foreach( $des_posts as $des_post ){
				    			$img2=get_attachment_image_src($des_post['ID'], 'full');
				        		$des_img=aq_resize($img2[0],119,124,true,true,true);
				        ?>
				        	<li>
				            	<div class="destination-image-wrapper" aria-haspopup="true">
			                        <a href="<?php echo get_permalink($des_post["ID"]);?>"><img src="<?php echo $des_img;?>" /></a>
				                </div>     
				                <h4 class="entry-title" data-fontsize="18" data-lineheight="27"><a href="<?php echo get_permalink($des_post["ID"]); ?>"> <?php echo $des_post["post_title"];?></a></h4>                     
				        	</li>
			        	<?php } ?>
					</ul>
				</div>
				<div class="read-more-1">			<a href="/<?php echo MH_DESTINATION;?>/"class="read-more">View All</a></div>
		</div>
	</div>
	<div class="row content-bt">
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
			<h4 class="title-border">Air Ticketing</h4>
			<?php
				$air_args = array(
				        'post_type' => 'page',
				        'post_parent' => MH_AIR_TICKETING
				    );
			    $air_childs = get_posts( $air_args );
			    foreach($air_childs as  $air_key=>$air_child) {
			    	$air_imgs=get_attachment_image_src($air_child->ID, 'full');
			    	$air_img=aq_resize($air_imgs[0],71,47,true,true,true);  
			    	$air_per=get_permalink(MH_AIR_TICKETING);
			?>
				<div class="air-ticketing-<?php echo ($air_key==0)?'left':'right';?>">
					<a href="<?php echo $air_per;?>" title="<?php echo $air_child->post_title;?>">
						<img class="alignleft" src="<?php echo $air_img;?>" alt="<?php echo $air_child->post_title;?>" />
						<h4><?php echo $air_child->post_title;?></h4>
					</a>
					<?php echo $air_child->post_excerpt;?>
				</div>
			<?php } ?>
		</div>
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">		
				<?php $car_page = get_page(MH_CAR_RENTAL); ?>
				<?php 
                    $carimg = get_attachment_image_src($car_page->ID, 'full'); 
                    $car_img=aq_resize($carimg[0],351,241,true,true,true);              
                ?>
			<h4 class="title-border"><?php echo $car_page->post_title;?></h4>
			<div class="car-rental">
				<img class="alignleft" src="<?php echo $car_img;?>" alt="car-rental" />
				<div class="mask">
                    <a href="<?php echo get_permalink($car_page->ID)?>" class="wrapmask">
                      <span><i class="fa fa-link"></i></span>
                   </a>
                </div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">						
			<h4 class="title-border">Client Feedback</h4>
			<div class="cd-testimonials-wrapper cd-container">
				<?php
					$testi_args = array(  
					    'post_type' => MH_TESTIMONIAL, 
					    'posts_per_page' => -1,
					    'orderby' => 'date',
					    'order' => 'DESC',
				    );
				    $list ="";
				    $testi_posts = wp_get_recent_posts( $testi_args );
			    ?>
			  	<ul class="cd-testimonials">
			  		<?php 
			    		foreach( $testi_posts as $testi_post ){
			        ?>
					    <li>
					      <p><?php echo $testi_post["post_content"];?>    </p>
					      <div class="cd-author">
					        <ul class="cd-author-info">
					          	<li><?php echo $testi_post["post_title"];?>    </li>
						        <li><?php echo get_field("country",$testi_post["ID"],true);?></li>
					        </ul>
					      </div>
					    </li>
				    <?php } ?>
			  	</ul>
			</div>
		</div>
	</div>
<?php get_footer(); ?>